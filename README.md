# IntelliHybris

#### 介绍
国家重点研发计划项目高效能云计算数据中心关键技术与装备（多元化投入试点）课题二任务需求

#### 软件架构
软件架构说明


智能有线无线混合调度软件
项目包含如下子模块：

platform

profiler

workload

代码构成说明
各目录内容如下：

platform：软件主界面模块

profiler：业务测量模块

workload：业务生成模块

部署步骤：


准备工作：
安装CentOS7系统

禁用、关闭防火墙
systemctl disable firewalld
systemctl stop firewalld


步骤：

一、部署k8s集群并安装依赖软件：

1、安装docker, yum install docker 
2、安装kubernetes, yum install kubelet kubectl kubeadm, 部署kubernetes集群
3、安装Qt 
4、安装python2.7以及pip包管理工具， 执行pip install psutil numpy pyyaml


二、软件运行

1、将profiler和workload文件夹copy到容器下，打包成docker镜像
2、用Qt代开platform文件夹下的工程，运行软件；


以上步骤全部成功，则混合有线无线调度软件部署成功。
