#!/usr/bin/python
# -*- coding: utf-8 -*-
# @Time    : 2021/07/10 10:30
# @Author  : Binquan Guo
# @Email   : 13468897661@163.com
# @Profile : https://wilixx.github.io
# @File    : network_produce.py
# @Function: To produce network load in a pod.

import subprocess
import numpy as np
import time
import psutil


if __name__ == "__main__":
    """ Make sure you have installed iperf2 on both machine,
    and environment variable added to system path """

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("-m", "--mean",
                       help="to monitor the net usage in a pod every few second",
                       type=str, default="400")

    parser.add_argument("-v", "--variance",
                       help="to query current net usage",
                       type=str, default="50")

    parser.add_argument("-t", "--time",
                       help="the duration to send traffic",
                       type=str, default=" 1 ")

    parser.add_argument("-d", "--dest", help="send to which ip address",
                        type=str, default=" 192.168.50.10 ")

    parser.add_argument("-p", "--port", help="send to destination port",
                        type=str, default=" 222 ")

    parser.add_argument("-P", "--Parallel_num", help="send to destination port",
                        type=str, default=" 1 ")

    args = parser.parse_args()

    # To start the client on the destination even if it has been started.

    try:
        proc = subprocess.Popen("iperf3 -s -p 222 ",
                            shell=True, stdin=None, stdout=None)
    except:
        print("it already started ! ")
        # pass


    ip_address     =      args.dest
    port           =      args.port
    duration_time  =      args.time
    Parallel_num   =      args.Parallel_num
    mean           =      args.mean
    variance       =      args.variance

    while True:
        current_bandwidth = "300M"  # default value or format

        # np.random.seed(0)
        s = np.abs(np.round(np.random.normal(int(mean), int(variance), 1)))

        # print("Gaussian: " + str(s[0]) + "M")
        current_bandwidth = " " + str(s[0]) + "M "

        try:
            cmdline_statement = r' iperf3  -c ' + ip_address + "  -p  " + port +"  -t " + duration_time + " -b  " + current_bandwidth + " -P " + Parallel_num

            proc = subprocess.Popen(cmdline_statement, shell=True, stdin=None, stdout=None)

            # out, err = proc.communicate()
            # print(" out, err :", out, err)
            print("End : %s" % time.ctime())
            print("|-----------  Executing : " + cmdline_statement + "---------|")
            time.sleep(1)
        except:
            pass

