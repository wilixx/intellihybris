import multiprocessing, time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

count = multiprocessing.cpu_count()
print('cpu count : ', count)

# exit()

def loop_x():
    while True:
        x = 1

def loop(percent=0.5):
    while True:
        busy_time, idle_time = 0.91, 0.09
        print("busy_time,idle_time:", busy_time, idle_time)
        start_time = time.time()
        x = 0
        while time.time() - start_time < busy_time:
            pass
        time.sleep(idle_time)

if __name__ == '__main__':

    count = multiprocessing.cpu_count()
    with ProcessPoolExecutor(max_workers=count-1) as pool:
       for j in range(count):
           # print(j)
           pool.submit(loop)
