import psutil
import time

while True:
  net_old = psutil.net_io_counters().bytes_sent
  time.sleep(1)

  net_new = psutil.net_io_counters().bytes_sent
  net_diff = (net_new - net_old) / 1024 / 1024 * 8 
  print("Network usage: "+ str(net_diff) + " Mbits/sec")

