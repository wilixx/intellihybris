#!/usr/bin/python
# -*- coding: utf-8 -*-
# @Time    : 2021/07/10 10:30
# @Author  : Binquan Guo
# @Email   : 13468897661@163.com
# @Profile : https://wilixx.github.io
# @File    : network_probe.py
# @Function: To probe network usage in a pod.

import subprocess
import numpy as np
import time
import psutil

def query_net_usage(sample_num=1):

    samples = []
    for i in range(sample_num):
        net_old = psutil.net_io_counters().bytes_sent
        time.sleep(1.0/sample_num)
        net_new = psutil.net_io_counters().bytes_sent

        net_count = float(net_new - net_old) * 8 / 1024 / 1024  # unit: M bits/s
        samples.append(net_count*sample_num)
        # average = np.array(samples).mean()
        # std = np.array(samples).std()
        # max = np.array(samples).max()
        max = np.array(samples).max()

    return max

def monitor_net_usage(interval=1):
    while True:
        net_old = psutil.net_io_counters().bytes_sent
        time.sleep(interval)
        net_new = psutil.net_io_counters().bytes_sent
        net_count = (net_new - net_old) * 8 / 1024 / 1024  # unit: M bits/s
        print("Network usage: " + str(net_count) + " M bit/s")

if __name__ == "__main__":
    """ Make sure you have installed iperf2 on both machine,
    and environment variable added to system path """

    import argparse

    parser = argparse.ArgumentParser()

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-m", "--monitor",
                       help="to monitor the net usage in a pod every few second",
                       action="store_true")

    group.add_argument("-q", "--query",
                       help="to query current net usage",
                       action="store_true", default=False)

    args = parser.parse_args()

    # To start the client on the destination

    if args.query == True:

        # default value is 10s, 1 time/s
        max = query_net_usage(sample_num=10)
        print("max: ", max)

    else:
        monitor_net_usage(interval=1)    # default value is 1s











