#ifndef DETECT_H
#define DETECT_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QTimer>
#include <QUdpSocket>
#include <QByteArray>
#include <QFileDialog>
#include <QUdpSocket>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <QProcess>
#include <QtCore/QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QTextCodec>
#include<QTime>
#include<QtCharts>
#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QMdiSubWindow>

class detect : public QObject
{
    Q_OBJECT
public:

    explicit detect(QObject *parent = nullptr);
    void detect_message_interrupt();
    void usr_detect_process();

    void get_usr_demand();
    void put_demand_to_struct();
    void display_demand();
    void run_scheduling();
    void renew_struct_array();
    void display_scheduling_results();
    void identify_and_estimate();


signals:

public slots:
    void judge_type();
private:
   QTimer* my_timer;





};

#endif // DETECT_H
