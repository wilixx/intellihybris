#include "clientwidget.h"
#include "ui_clientwidget.h"
#include<QHostAddress>
#include <QDebug>
#include <QMainWindow>
#include "detect.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <QProcess>
#include <QtCore/QCoreApplication>
#include <QFile>
#include <QTextCodec>
#include<QTime>
#include<QtCharts>
#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QMdiSubWindow>
#include "qcustomplot.h"
#include <math.h>
#include <QMessageBox>





ClientWidget::ClientWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClientWidget)
{
    ui->setupUi(this);

    //初始化套接字
    tcpSocket = NULL;

    //分配空间，指定父对象
    tcpSocket = new QTcpSocket(this);

    //建立连接的信号槽
    connect(tcpSocket,&QTcpSocket::connected,
            [=]()
            {
                 ui->textEditRead->setText("成功连接到任务管理模块！");
            }
            );

    //连接建立后收到服务器数据的信号槽
    connect(tcpSocket,&QTcpSocket::readyRead,
                [=]()
                {
                    //获取对方发送的内容
                    QByteArray array = tcpSocket->readAll();

                    //显示到编辑框
                    ui->textEditRead->append(array);//append，添加内容
                }
            );

    //设置窗口标题
    setWindowTitle("用户任务需求模块");


    //启动服务器
    //获取服务器IP和端口
    QString ip = "127.0.0.1";
    qint16 port = 8888;

    //主动连接服务器
    tcpSocket->connectToHost(QHostAddress(ip),port);


    //发送数据
    //获取编辑框内容
     QString str ="2";//任务需求模块向任务管理模块反馈数据****************
     //发送数据
     tcpSocket->write( str.toUtf8().data() );
}

ClientWidget::~ClientWidget()
{
    delete ui;
}

void ClientWidget::on_buttonConnect_clicked()
{

}

void ClientWidget::on_buttonSend_clicked()
{


}

void ClientWidget::on_buttonClose_clicked()
{
    //主动断开连接
    tcpSocket->disconnectFromHost();
}

void ClientWidget::on_pushButton_clicked()
{
//    ////用户需求表
//        /* 创建表格视图 */
//        QTableView *tableView = new QTableView;
//        /* 设置表格视图大小 */
//    //    tableView->resize(850, 400);
//        /* 创建数据模型 */
//        QStandardItemModel* model = new QStandardItemModel();
//        /* 设置表格标题行(输入数据为QStringList类型) */
//        model->setHorizontalHeaderLabels({"ID", "User Name", "City", "Classify", "Score", "Sign"});
//        /* 自适应所有列，让它布满空间 */
//        tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//        /* 加载共10行数据，并每行有6列数据 */
//        for (int i = 0; i < 10; i++) {
//            /* 加载第一列(ID)数据 */
//            model->setItem(i, 0, new QStandardItem(QString("100%1").arg(i)));
//            /* 加载第二列(User Name)数据 */
//            model->setItem(i, 1, new QStandardItem(QString("User%1").arg(i)));
//            /* 加载第三列(City)数据 */
//            model->setItem(i, 2, new QStandardItem("Shanghai"));
//            /* 加载第四列(Classify)数据 */
//            model->setItem(i, 3, new QStandardItem("Engineer"));
//            /* 加载第五列(Score)数据 */
//            model->setItem(i, 4, new QStandardItem("80"));
//            /* 加载第六列(Sign)数据 */
//            model->setItem(i, 5, new QStandardItem("Hello world!"));
//        }
//        /* 设置表格视图数据 */
//        tableView->setModel(model);
//        /* 显示 */
////        tableView->show();
//        //使用QMdiArea类的addSubWindow函数来创建子窗口，以文本编辑器为中心部件
//        QMdiSubWindow* usr_demand_table = Ui::MainWindow-> mdiArea ->addSubWindow(tableView);
//        usr_demand_table->setWindowTitle("usr_demand_table window");
//        usr_demand_table->show();
//    //    this->setCentralWidget(ui->mdiArea);
//        usr_demand_table->setWindowState(Qt::WindowMaximized);

}
