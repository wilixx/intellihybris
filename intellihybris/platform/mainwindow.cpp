#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <QVector>
#include <numeric>
#include <QtCore/qmath.h>
#include <QtGlobal>
#include <QDir>
#include <QMessageBox>
#include <QByteArray>
#include <QFileDialog>
#include <QFile>
#include <streambuf>
#include <QTextStream>
#include <algorithm.h>
QVector <Application>application;
QVector <PM>PMachine;

QVector<int> Cross;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this); //显示总体
    init();      //初始化

    ROuterUdpSocket = new QUdpSocket(this);
    ROuterUdpSocket->bind(12346);
    connect(ROuterUdpSocket, SIGNAL(readyRead()), this, SLOT(nodeReceived()));

    //Update_Struct();
    connect(&timer_performance_test,SIGNAL(timeout()),this,SLOT(PerformanceTest_slot()));
    connect(&Updater,SIGNAL(timeout()),this,SLOT(Update_Struct()));
    sendpacketSocket = new QUdpSocket();
    recvpacketSocket = new QUdpSocket();
    recvpacketSocket->bind(9999);
    connect(&Updater,SIGNAL(timeout()),this,SLOT(InPut()));
    Updater.setInterval(50000);
    Updater.start();
    timer_performance_test.setInterval(2000);
    timer_performance_test.start();

    showWindow();//显示整体布局
    PolicyAlgorithm();     //决策算法
    OutPut();   //算法输出
    Reconfiguration();   //业务重构
    PerformanceTest();   //指标输出
    read_txt_function();//读文件
    //加载用户需求表
    fill_userTable();

    //box5的图2
    ui->widget_2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                    QCP::iSelectLegend | QCP::iSelectPlottables);
    //设定右上角图形标注可见
    ui->widget_2->legend->setVisible(true);
    //设定右上角图形标注的字体
    ui->widget_2->legend->setFont(QFont("Helvetica", 9));
    //添加图形
    ui->widget_2->addGraph();
    //设置画笔
    ui->widget_2->graph(0)->setPen(QPen(Qt::blue));
    //设置画刷,曲线和X轴围成面积的颜色
    //ui->widget_2->graph(0)->setBrush(QBrush(QColor(0,255,0)));
    //设置右上角图形标注名称
    ui->widget_2->graph(0)->setName("有线测量实时流量图");
    ui->widget_2->addGraph();
    //设置画笔
    ui->widget_2->graph(1)->setPen(QPen(Qt::red));
    //设置画刷,曲线和X轴围成面积的颜色
    ui->widget_2->graph(1)->setBrush(QBrush(QColor(0,255,0)));
    //设置右上角图形标注名称
    ui->widget_2->graph(1)->setName("无线测量实时流量图");
    //设置X轴文字标注
    ui->widget_2->xAxis->setLabel("时间t  second");
    //设置Y轴文字标注
    ui->widget_2->yAxis->setLabel("流量速率 Mbit/sec");
    //设置X轴坐标范围
    ui->widget_2->xAxis->setRange(0,1000);
    //设置Y轴坐标范围
    ui->widget_2->yAxis->setRange(smallest-5,biggest+5);
    //在坐标轴右侧和上方画线，和X/Y轴一起形成一个矩形
    ui->widget_2->axisRect()->setupFullAxesBox();

    //box1的图3
    ui->widget_3->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                    QCP::iSelectLegend | QCP::iSelectPlottables);
    //设定右上角图形标注可见
    ui->widget_3->legend->setVisible(true);
    //设定右上角图形标注的字体
    ui->widget_3->legend->setIconSize(QSize(2,1));
    ui->widget_3->legend->setFont(QFont("Helvetica", 6));
    ui->widget_3->legend->setIconSize(QSize(2,1));
    //添加图形
    ui->widget_3->addGraph();
    ui->widget_3->legend->setIconSize(QSize(2,1));
    //设置画笔
    ui->widget_3->graph(0)->setPen(QPen(Qt::blue));
    //设置画刷,曲线和X轴围成面积的颜色
    //ui->widget->graph(0)->setBrush(QBrush(QColor(255,255,0)));
    //设置右上角图形标注名称
    ui->widget_3->graph(0)->setName("实时均值统计图");
    //添加图形
    ui->widget_3->addGraph();
    //设置画笔
    ui->widget_3->graph(1)->setPen(QPen(Qt::red));
    //设置画刷,曲线和X轴围成面积的颜色
    //ui->widget->graph(1)->setBrush(QBrush(QColor(0,255,0)));
    //设置右上角图形标注名称
    ui->widget_3->graph(1)->setName("前100秒的均值");
    //设置X轴文字标注
    ui->widget_3->xAxis->setLabelFont(QFont("Helvetica",6));
    ui->widget_3->xAxis->setLabel("时间t  second");
    //设置Y轴文字标注
    ui->widget_3->yAxis->setLabelFont(QFont("Helvetica",6));
    ui->widget_3->yAxis->setLabel("流量速率 Mbit/sec");
    //设置X轴坐标范围
    ui->widget_3->xAxis->setRange(0,1000);
    //设置Y轴坐标范围
    ui->widget_3->yAxis->setRange(0,30);
    //在坐标轴右侧和上方画线，和X/Y轴一起形成一个矩形
    ui->widget_3->axisRect()->setupFullAxesBox();

    //boxing2 图4
    ui->widget_4->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                    QCP::iSelectLegend | QCP::iSelectPlottables);
    //设定右上角图形标注可见
    ui->widget_4->legend->setVisible(true);
    //设定右上角图形标注的字体
    ui->widget_4->legend->setFont(QFont("Helvetica", 6));
    ui->widget_4->legend->setIconSize(6,3);
    //添加图形
    ui->widget_4->addGraph();
    //设置画笔
    ui->widget_4->graph(0)->setPen(QPen(Qt::blue));
    //设置画刷,曲线和X轴围成面积的颜色
    //ui->widget_4->graph(0)->setBrush(QBrush(QColor(255,255,0)));
    //设置右上角图形标注名称
    ui->widget_4->graph(0)->setName("实时标准差统计图");
    //添加图形
    ui->widget_4->addGraph();
    //设置画笔
    ui->widget_4->graph(1)->setPen(QPen(Qt::red));
    //设置画刷,曲线和X轴围成面积的颜色
    //ui->widget_4->graph(1)->setBrush(QBrush(QColor(0,255,0)));
    //设置右上角图形标注名称
    ui->widget_4->graph(1)->setName("前100秒的标准差");
    //设置X轴文字标注
    ui->widget_4->xAxis->setLabelFont(QFont("Helvetica",6));
    ui->widget_4->xAxis->setLabel("时间t  second");
    //设置Y轴文字标注
    ui->widget_4->yAxis->setLabelFont(QFont("Helvetica",6));
    ui->widget_4->yAxis->setLabel("流量速率 Mbit/sec");
    //设置X轴坐标范围
    ui->widget_4->xAxis->setRange(0,1000);
    //设置Y轴坐标范围
    ui->widget_4->yAxis->setRange(0,20);
    //在坐标轴右侧和上方画线，和X/Y轴一起形成一个矩形
    ui->widget_4->axisRect()->setupFullAxesBox();

   //任务部署表
    /* 创建表格视图 */
    QTableView *tableView_task = new QTableView;
    /* 设置表格视图大小 */
//    tableView_task->resize(850, 400);
    /* 创建数据模型 */
    QStandardItemModel* model_task = new QStandardItemModel();
    /* 设置表格标题行(输入数据为QStringList类型) */
    model_task->setHorizontalHeaderLabels({"pod数量总和", "CPU资源利用率", "服务器测试峰值", "CPU容量", "CPU映射资源利用率", "Pod测试峰值之和","CPU超卖比",
                                          "业务CPU需求值","实际分配CPU值","网络资源利用率","带宽测试峰值","带宽容量","网络映射资源利用率","Pod带宽占用测试峰值之和",
                                          "网络超卖比","业务带宽需求值","实际分配带宽"});
    /* 自适应所有列，让它布满空间 */
    tableView_task->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    /* 加载共10行数据，并每行有9列数据 */
    for (int i = 0; i < 10; i++) {
        /* 加载第一列(ID)数据 */
        model_task->setItem(i, 0, new QStandardItem(QString("%1").arg((i+2)*5)));
        /* 加载第二列(User Name)数据 */
        model_task->setItem(i, 1, new QStandardItem(QString("%1Mbit/s").arg((i+3)*4)));
        /* 加载第三列(City)数据 */
        model_task->setItem(i, 2, new QStandardItem(QString("%1").arg((i+2)*2)));
        /* 加载第四列(Classify)数据 */
        model_task->setItem(i, 3, new QStandardItem(QString("%1Mbit/s").arg((i+2)*3)));
        /* 加载第五列(Score)数据 */
        model_task->setItem(i, 4, new QStandardItem(QString("%1%").arg((i*3)+40)));
        /* 加载第六列(Sign)数据 */
        model_task->setItem(i, 5, new QStandardItem(QString("%1%").arg((i*4)+26)));
        /* 加载第四列(Classify)数据 */
        model_task->setItem(i, 6, new QStandardItem(QString("%1Mbit/s").arg((i+2)*3)));
        /* 加载第五列(Score)数据 */
        model_task->setItem(i, 7, new QStandardItem(QString("%1%").arg((i*3)+40)));
        /* 加载第六列(Sign)数据 */
        model_task->setItem(i, 8, new QStandardItem(QString("%1%").arg((i*4)+26)));
    }
    /* 设置表格视图数据 */
    tableView_task->setModel(model_task);
    /* 显示 */
//    tableView->show();
    //使用QMdiArea类的addSubWindow函数来创建子窗口，以文本编辑器为中心部件
    QMdiSubWindow* usr_demand_table_task = ui->mdiArea_2->addSubWindow(tableView_task);
    usr_demand_table_task->setWindowTitle("usr_demand_table window");
    usr_demand_table_task->show();
//    this->setCentralWidget(ui->mdiArea);
    usr_demand_table_task->setWindowState(Qt::WindowMaximized);
}

MainWindow::~MainWindow()
{
    //delete ui;
}

//显示界面布局
void MainWindow::showWindow(){
    this->showMaximized();
    this->setAutoFillBackground(true);
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background,QColor(0xc6,0xe2,0xff)); //更改 9.1
    this->setPalette(palette);
    this->setWindowOpacity(1.0);

    /*
     * @author by zh
     */
    ui->textEdit_5->setAutoFillBackground(true);
    QPalette palette2 = ui->textEdit_5->palette();
    //palette2.setColor(QPalette::Base,QColor(0x36,0x36,0x36));
    palette2.setColor(QPalette::Base,QColor(0xff,0xff,0xff));
    ui->textEdit_5->setPalette(palette2);
    ui->textEdit_5->setWindowOpacity(1.0);

    ui->textEdit_6->setAutoFillBackground(true);
    QPalette palette3 = ui->textEdit_6->palette();
    palette3.setColor(QPalette::Base,QColor(0xff,0xff,0xff));
    ui->textEdit_6->setPalette(palette3);
    ui->textEdit_6->setWindowOpacity(1.0);
    ui->textEdit_7->setAutoFillBackground(true);
    QPalette palette4 = ui->textEdit_7->palette();
    palette4.setColor(QPalette::Base,QColor(0xff,0xff,0xff));
    ui->textEdit_7->setPalette(palette4);
    ui->textEdit_7->setWindowOpacity(1.0);


    //显示底线
    ui->statusBar->showMessage("Powered by 张晗 刘文慧 刘兆建 李媛媛 张可涵 葛云峰",-1);
    QLabel* permanent = new QLabel(this);
    permanent->setFrameStyle(QFrame::Box|QFrame::Sunken);
    permanent->setTextFormat(Qt::RichText);
    permanent->setText("©2021 业务流建模. All rights reserved.");
    ui->statusBar->addPermanentWidget(permanent);
}

//加载用户需求表
void MainWindow::fill_userTable(){
    ui->tableUser->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        for(int i = 0; i < application.size(); i++){
             qDebug()<<"****************"<<endl;
             Application obj = application[i];
             //添加新行
             ui->tableUser->insertRow(ui->tableUser->rowCount());
             int rowIdx = ui->tableUser->rowCount()-1;
             //必须先设置item,然后再获取,因为默认是空的
    //         QTableWidgetItem *item0 = new QTableWidgetItem(obj.ID);
    //         QTableWidgetItem *item1 = new QTableWidgetItem(obj.Req_CPU);
    //         QTableWidgetItem *item2 = new QTableWidgetItem(obj.Req_Band);
             QTableWidgetItem *item0 = new QTableWidgetItem("cawee");
             QTableWidgetItem *item1 = new QTableWidgetItem("2");
             QTableWidgetItem *item2 = new QTableWidgetItem("3");
             ui->tableUser->setItem(rowIdx,0,item0);
             ui->tableUser->setItem(rowIdx,1,item1);
             ui->tableUser->setItem(rowIdx,2,item2);
        }
}

void MainWindow::slot_timeout()
{
}
void MainWindow::read_txt_function()
{
    //读取data数据
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QFile file("D:\\Qt\\qt\\Normaltest.txt");
    //QFile file("/home/hcew1234.txt");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug()<<"Can't open the file!"<<endl;
    }
    while(!file.atEnd()) {
        QByteArray line = file.readLine();
        QString str(line);
        str.remove("\n");
        reserved_data.append(str.toInt());
    }
   // qDebug()<<"reserved_data:"<<reserved_data<<endl;

    //qDebug()<<"reserved_data[0]:"<<reserved_data[0]<<endl;
    auto max = std::max_element(std::begin(reserved_data), std::end(reserved_data));
         //最小值表示：
    auto min = std::min_element(std::begin(reserved_data), std::end(reserved_data));
         //直接赋值表示
    biggest = *max;
    smallest = *min;
    qDebug()<<"biggest = "<<biggest;
    qDebug()<<"smallest = "<<smallest;

    mean_old_3 = reserved_data[0];
    mean_new_3 = (mean_old_3 + reserved_data[2])/2;
    var_old_3=reserved_data[1];
    var_new_3 = (qPow((mean_old_3 - mean_new_3 ),2) + qPow((reserved_data[2] - mean_new_3 ),2))/2;
    qDebug()<<"mean_old_3:"<<mean_old_3<<endl;
    qDebug()<<"mean_new_3:"<<mean_new_3<<endl;
    qDebug()<<"var_old_3:"<<var_old_3<<endl;
    qDebug()<<"var_new_3:"<<var_new_3<<endl;

    mean_old_4 = reserved_data[0];
    mean_new_4 = (mean_old_4 + reserved_data[2])/2;
    var_old_4=reserved_data[1];
    std_old_4 = qSqrt(var_old_4);
    var_new_4 = (qPow((mean_old_4 - mean_new_4 ),2) + qPow((reserved_data[2] - mean_new_4 ),2))/2;
    std_new_4 = qSqrt(var_new_4);
    qDebug()<<"mean_old_4:"<<mean_old_4<<endl;
    qDebug()<<"mean_new_4:"<<mean_new_4<<endl;
    qDebug()<<"var_old_4:"<<var_old_4<<endl;
    qDebug()<<"var_new_4:"<<var_new_4<<endl;

    mean_old_5 = reserved_data[0];
    mean_new_5 = (mean_old_5 + reserved_data[2])/2;
    var_old_5=reserved_data[1];
    var_new_5 = (qPow((mean_old_5 - mean_new_5 ),2) + qPow((reserved_data[2] - mean_new_5 ),2))/2;
    qDebug()<<"mean_old_5:"<<mean_old_5<<endl;
    qDebug()<<"mean_new_5:"<<mean_new_5<<endl;
    qDebug()<<"var_old_5:"<<var_old_5<<endl;
    qDebug()<<"var_new_5:"<<var_new_5<<endl;

    mean_old_6 = reserved_data[0];
    mean_new_6 = (mean_old_6 + reserved_data[2])/2;
    var_old_6=reserved_data[1];
    std_old_6 = qSqrt(var_old_6);
    var_new_6 = (qPow((mean_old_6 - mean_new_6 ),2) + qPow((reserved_data[2] - mean_new_6 ),2))/2;
    std_new_6 = qSqrt(var_new_6);
    qDebug()<<"mean_old_6:"<<mean_old_6<<endl;
    qDebug()<<"mean_new_6:"<<mean_new_6<<endl;
    qDebug()<<"var_old_6:"<<var_old_6<<endl;
    qDebug()<<"var_new_6:"<<var_new_6<<endl;
}

QList<int> reserved_data;
QList<int>  reserved_data_frequency;
QList<int>  get_data_frequency;

void MainWindow::slot_timeout_2()
{
    //传入数据
    ui->widget_2->graph(0)->addData(x,reserved_data[i]);
    QList<int> wlanList;
    for(int j :reserved_data ){
        wlanList.append(j/10);
    }
    ui->widget_2->graph(1)->addData(x,wlanList[i]);

    QDateTime timeCur = QDateTime::currentDateTime();//获取系统现在的时间
    QString timeStr = timeCur.toString("yyyy-MM-dd hh:mm:ss ddd"); //秒
    /*
    QTime t ;
    t=QTime::currentTime();
    qsrand(QTime(0,0,0,0).secsTo(QTime::currentTime()));
    int n = qrand() % 100 + 100;*/


    ui->textEdit->setText(timeStr+"\r\n当前有线测量到的速率为: "+QString::number(reserved_data[i])+"Mbit/sec" + "\r\n当前无线测量到的速率为:" + QString::number(reserved_data[i]/10) + "Mbit/sec");
    if(x==100){
    ui->textEdit_2->setText("估计当前业务模型为：高斯分布\r\n测量数据与高斯分布的偏离度为: 0.02399124840706677\r\n测量数据与泊松分布的偏离度为: 0.316309705683933515\r\n测量数据与帕累托分布的偏离度为: 0.48239960706116\r\n均值为:12\r\n标准差为：7");
    }
    ui->widget_2->replot();
    x=x+1;
    i = i+1;
    if(i==998){
         disconnect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_2()));
    }

}

void MainWindow::slot_timeout_3(){
    //传入数据
    ui->widget_3->graph(0)->addData(x_3,mean_new_3);
    ui->widget_3->graph(1)->addData(x_3,mean_old_3);
    x_3=x_3+1;
    i_3 = i_3+1;
    t_3= mean_new_3;
    mean_new_3 = (mean_new_3 + reserved_data[i_3])/2;
    var_new_3 = (qPow((t_3 - mean_new_3),2) + qPow((reserved_data[i_3] - mean_new_3 ),2))/2;
    sum_3 =sum_3 + qAbs(var_new_3 - var_old_3);
    if(i==998){
         disconnect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_3()));
    }
    if(x>100){
        ui->widget_3->replot();
    }
    if(x==996){
        ui->textEdit_3->setText("均值时间统计平均值："+QString::number(sum_3/996));
    }
}

void MainWindow::slot_timeout_4()
{
    //传入数据
    ui->widget_4->graph(0)->addData(x_4,std_new_4);
    ui->widget_4->graph(1)->addData(x_4,std_old_4);
    x_4=x_4+1;
    i_4 = i_4+1;
    t_4= mean_new_4;
    mean_new_4 = (mean_new_4 + reserved_data[i_4])/2;
    var_new_4 = (qPow((t_4 - mean_new_4 ),2) + qPow((reserved_data[i_4] - mean_new_4 ),2))/2;
    std_new_4 = qSqrt(var_new_4);
    sum_4 =sum_4 + qAbs(std_new_4 - std_old_4);
    if(i==998){
         disconnect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_4()));
    }
    if(x>100){
         ui->widget_4->replot();
    }
    if(x==996){
        ui->textEdit_4->setText("标准差时间统计平均值："+QString::number(sum_4/996));
    }

}
void MainWindow::on_pushButton_clicked()
{


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout()));
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_2()));
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_3()));
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timeout_4()));
    //柱状图函数
    SimpleDemo();

    timer->start(10);
}

void MainWindow::SimpleDemo()
{
    QVector<QString> labels(5);
       QVector<double> values(5);
       QTime t;
       t=QTime::currentTime();
       qsrand(t.msec()+t.second()*1000);
       //n=qrand()%25+5;

       for(int i=0;i<5;++i)
           values[i]=qrand()%25+1;
       values[4]=0;
       labels[0]=QString("需求值");
       labels[1]=QString("分配值");
       labels[2]=QString("均值");
       labels[3]=QString("方差值");

       QCPBars* bars=new QCPBars(this->ui->widget->xAxis,this->ui->widget->yAxis);
       //bars->brush();
       this->ui->widget->clearPlottables();

           QVector<double> index(5);
           for(int i=0;i<5;++i)
               index[i]=i;
           bars->setData(index,values);


       //QCPBars的setData()的两个参数也是两个向量，只不过第一个向量index的每个元素表示“第几个柱子”，然后后面对应的values表示对应“柱子的值”
       //添加完了绘制的柱状图，接下来添加标签，要想完全自己定义标签，需要先执行以下代码关闭默认的底部标签自动生成


       this->ui->widget->xAxis->setAutoTicks(false);
           this->ui->widget->xAxis->setAutoTickLabels(false);
           this->ui->widget->xAxis->setAutoTickStep(false);
       //先将bars添加到widget上吧，然后自动调整下坐标系

       this->ui->widget->addPlottable(bars);
           this->ui->widget->rescaleAxes();


       //接下来我们要生成我们标签的位置坐标，有个公式计算，其生成的向量 coor里面就对应了我的labels的坐标，注意这个坐标不是屏幕像素坐标，而是它这个坐标系的坐标，如果不是很明白就把上面的那三个false改为true看下它默认的坐标。
       double wid=this->ui->widget->xAxis->range().upper-this->ui->widget->xAxis->range().lower;
           double cl=bars->width()+(1.0*wid-bars->width()*5)/4;

           QVector<double> coor;
           for(int i=0;i<5;++i)
               coor.append(this->ui->widget->xAxis->range().lower+i*cl+bars->width()/2);
           this->ui->widget->xAxis->setTickVector(coor);
           this->ui->widget->xAxis->setTickVectorLabels(labels);


       //最后replot一下
       this->ui->widget->replot();
       //SimpleDemo(this->ui->widget,tempnum,n);
}

//与用户软件通信模块
void MainWindow::nodeReceived()
{
    QHostAddress addr;
    quint16 port;
    char buf[10240];
    qDebug() << "接收到数据报文" << endl;
    int len = ROuterUdpSocket->readDatagram(buf, 10240, &addr, &port);

    qDebug() << "接收到长度为" << len << "的数据包，[原地址 =" << addr.toString() << ", 端口 = " << port << "]" << endl;

    Application* msg = (Application*) buf;


//        Application a;
//        memcpy(&a, buf, len);
        qDebug()<<" "<<msg->Req_CPU<<endl;
        qDebug()<<" "<<msg->Estimate_Band_mean<<endl;
        qDebug()<<"msg->Req_Band_mean =  "<<msg->Req_Band_mean<<endl;
        //qDebug()<<"msg->list =  "<<msg->list<<endl;
        //qDebug()<<" "<<msg->location<<endl;
}

/*
决策算法模块
*/

//定时器
void MainWindow::Update_Struct(){
    qDebug() << "|---------------------------------------------------timeCounter: "<< timeCounter << endl;
    PolicyAlgorithm();     //决策算法
    OutPut();   //算法输出
    Reconfiguration(); //业务重构
    PerformanceTest();   //指标输出
    timeCounter  += 1;
}

//初始化
void MainWindow::init(){
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    //qsrand(10);
    int App_Num = 4;   //应用数量
    for(int i=0; i<App_Num; i++){ //对每个应用赋值
        Application a1;

        a1.Req_CPU = qrand()%20+30; //每个应用的CPU需求，范围在[30,50]内
        a1.Req_CPU_mean = qrand()%10+20; //应用初始的CPU均值，范围在[20,30]内
        a1.Req_CPU_var = qrand()%8; //应用初始方差的CPU方差，范围在[0,8^2]内

        a1.Req_Band = qrand()%50+30; //每个应用的带宽需求，范围在[30,80]内
        a1.Req_Band_mean = qrand()%10+20; //应用初始的带宽均值，范围在[20,30]内
        a1.Req_Band_var = qrand()%8; //应用初始的带宽方差，范围在[0,8^2]内

        a1.Estimate_CPU_mean = a1.Req_CPU; //初始估计值，默认为应用CPU需求值
        a1.Estimate_CPU_var = 0; //初始化为0
        a1.Estimate_Band_mean = a1.Req_Band; //初始估计值，默认为应用带宽需求值
        a1.Estimate_Band_var = 0; //初始化为0

       // a1.Measure_Band = qrand()%10+20; //初始还没有测量数据，默认为0 /[20,60]
       // a1. = qrand()%10+20; //初始还没有测量数据，默认为0  /[30,50]

        a1.Real_CPU = a1.Req_CPU; //实际给应用分配的资源，初始为应用需求
        a1.Real_Band = a1.Req_Band; //实际给应用分配的资源，初始为应用需求

        //a1.location = "";  //应用部署位置，初始化为空; 不定义系统默认是空的
        application.append(a1);
    }

    int PM_Num = 3;        //物理机数量
    for(int i=0; i<PM_Num; i++){ //初始化物理机信息
        PM p1;
        p1.Capacity_CPU = 1000;  //初始默认物理机CPU总量为100
        p1.Available_CPU = p1.Capacity_CPU; //初始物理机可用CPU资源等于物理机CPU总量

        p1.Capacity_Band = 1000; //初始默认物理机Band总量为100
        p1.Available_Band = p1.Capacity_Band; //初始物理机可用Band资源等于物理机Band总量

        p1.conncetPort = i;     //物理机连接的交换机端口号

        //p1.IP = QString("192.168.10.") +  QString::number(i+10); //物理机的IP

        PMachine.append(p1);

        //PMachine[i].pod = "";    //初始物理机还没有承载应用，默认内部创建的pod为空
    }
        PMachine[0].IP = "192.168.10.98";
        PMachine[0].Name = "98";

        PMachine[1].IP = "192.168.10.99";
        PMachine[1].Name = "99";

        PMachine[2].IP = "192.168.10.100";
        PMachine[2].Name = "100";
}


//测量结果输入
void MainWindow::InPut(){

    qDebug()<<"Measure data receiving";
    //恢复成发前的包
    QHostAddress addr;
    quint16 port;
    char buf[10240] = {0};
    int len = 0;
    if(recvpacketSocket->hasPendingDatagrams()){
        len = recvpacketSocket->readDatagram(buf,10240,&addr,&port);
        qDebug()<<"data received.. ";
    }else
    {
        qDebug()<<"no data received yet.. ";
    }

    qDebug()<<"len:"<<len;

    int b;
    memcpy(&b,buf,len);
    buf[10240] = {0};
    qDebug()<<"receive value:"<<b;

    for(int i=0; i<application.size(); i++){
        //qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
        Application a1;
        a1.Measure_Band = b ; //初始还没有测量数据，默认为0 /[20,60]
        a1.Measure_CPU = qrand()%10+10; //初始还没有测量数据，默认为0  /[30,50]

        application[i].Measure_Band = a1.Measure_Band;
        application[i].Measure_CPU = a1.Measure_CPU;
  }
}

//业务参数估计
void MainWindow::Estimate(){

    for(int i=0;i<application.size();i++){

    //估计应用CPU实际占用的资源均值、方差
    application[i].Estimate_CPU_mean = (application[i].Estimate_CPU_mean + application[i].Measure_CPU)/2;
    application[i].Estimate_CPU_var = sqrt((application[i].Estimate_CPU_var + pow((application[i].Measure_CPU-application[i].Estimate_CPU_mean),2)));

    //估计应用带宽均值、方差
    application[i].Estimate_Band_mean = (application[i].Estimate_Band_mean + application[i].Measure_Band)/2;
    application[i].Estimate_Band_var = sqrt((application[i].Estimate_Band_var + pow((application[i].Measure_Band-application[i].Estimate_Band_mean),2)));

    //估计应用的新CPU、带宽需求
    application[i].Real_CPU = application[i].Estimate_CPU_mean + 3*application[i].Estimate_CPU_var;
    application[i].Real_Band = application[i].Estimate_Band_mean + 3*application[i].Estimate_Band_var;
  }
}

//决策算法
void MainWindow::PolicyAlgorithm(){

    for(int i=0;i<application.size();i++){
        for(int j=0;j<PMachine.size();j++){

            if((application[i].location).isEmpty()){  //如果应用i还没有部署，则进行第一次部署；
                //统计物理机已经被使用的带宽、CPU资源
                double PMUsedBand,PMUsedCPU;
                //当前物理机占用资源归零，以便统计下一循环物理机被占用资源
                PMUsedBand = 0;
                PMUsedCPU = 0;
                for(int k=0;k<PMachine[j].pod.size();k++){
                    PMUsedCPU = PMUsedCPU+PMachine[j].pod[k].CPU;   //统计物理机已经使用的CPU资源
                    PMUsedBand = PMUsedBand+PMachine[j].pod[k].Band;  //统计物理机已经使用的带宽资源
                }
                PMachine[j].Available_CPU = PMachine[j].Capacity_CPU - PMUsedCPU; //统计物理机可用的剩余CPU资源
                PMachine[j].Available_Band = PMachine[j].Capacity_Band - PMUsedBand; //统计物理机可用的剩余带宽资源

                //判断物理机j的剩余资源是否满足当前应用i
                if(application[i].Req_CPU<=PMachine[j].Available_CPU && application[i].Req_Band<=PMachine[j].Available_Band){
                    application[i].location = PMachine[j].IP;  //将应用i的部署位置标志为物理机j的IP

                    //在物理机j中创建pod
                    Pod p2;
                    p2.CPU = application[i].Req_CPU; //pod申请的CPU大小为应用i的CPU大小
                    p2.Band = application[i].Req_Band; //pod申请的带宽大小为应用i的带宽大小
                    p2.ID = QString::number(i);
                    p2.IP = PMachine[j].Name;
                    PMachine[j].pod.append(p2);
                    break;
                }
            }

            else{   //如果不是第一次部署，对应用需求先进行估计再部署
                // InPut();
                // sleep(0.5);
                Estimate();
                //判断新的需求值是否有变化
                if(application[i].Real_CPU != application[i].Req_CPU || application[i].Real_Band != application[i].Req_Band ){
                   //有变化，则需要重新部署
                    for(int k=0;k<PMachine[j].pod.size();k++){
                        if(PMachine[j].pod[k].ID == QString::number(i)){
                            //释放应用i原先占用的CPU、带宽资源

                            PMachine[j].pod.remove(k);


                            /* PMachine[j].pod[k].Band = 0;
                            PMachine[j].pod[k].CPU = 0;
                            PMachine[j].pod[k].IP = "";
                            PMachine[j].pod[k].ID = ""; */
                        }
                    }
                    //重新部署

                        double PMUsedBand,PMUsedCPU;
                        PMUsedBand = 0;
                        PMUsedCPU = 0;
                        for(int k=0;k<PMachine[j].pod.size();k++){ //物理机已经被使用的带宽、CPU资源

                            PMUsedCPU = PMUsedCPU+PMachine[j].pod[k].CPU;   //物理机已经使用的CPU资源
                            PMUsedBand = PMUsedBand+PMachine[j].pod[k].Band;  //物理机已经使用的带宽资源
                        }

                        PMachine[j].Available_CPU = PMachine[j].Capacity_CPU - PMUsedCPU; //物理机可用的剩余CPU资源
                        PMachine[j].Available_Band = PMachine[j].Capacity_Band - PMUsedBand; //物理机可用的剩余带宽资源

                        //判断物理机j的剩余资源是否满足当前应用i
                        if(application[i].Real_CPU<=PMachine[j].Available_CPU && \
                                application[i].Real_Band<=PMachine[j].Available_Band){

                            application[i].location = PMachine[j].IP;  //将应用i的部署位置标志为物理机j的IP

                            //在物理机j中创建pod
                            Pod p2;
                            p2.CPU = application[i].Real_CPU; //pod申请的CPU大小为应用i的CPU大小
                            p2.Band = application[i].Real_Band; //pod申请的带宽大小为应用i的带宽大小
                            p2.ID = QString::number(i);
                            p2.IP = PMachine[j].Name;
                            PMachine[j].pod.append(p2);
                            break;
                        }
                    }
                }

            }
        }
}

//决策算法输出
void MainWindow::OutPut(){

    //估计的业务均值、方差发送给node节点
    qDebug()<<"send to iperf information ="<<application[1].Estimate_Band_mean;
    sendpacketSocket->writeDatagram(QByteArray((char*)&application[1].Estimate_Band_mean,sizeof(double)),sizeof(double),QHostAddress("192.168.50.12"),10000);

    //决策结果输出为txt文件，业务重构模块调用
    qDebug()<<"send Pod information"<<endl;
    QFile output_file_forPod("E:\\code\\qt\\satData.txt");

    //QFile ("/root/Desktop/satData.txt");
    if (output_file_forPod.exists())
    {
        output_file_forPod.remove();

    }
    if (!output_file_forPod.open(QIODevice::WriteOnly|QIODevice::Text|QFile::Append))
    {
        qDebug()<<"打开satData.txt文件失败，无法写入"<<endl;//打开文件失败,应触发报警信号,直接返回//To-do
        return;
    }
    QTextStream out(&output_file_forPod);
    out << "{" <<endl;
    for(int i =0; i<PMachine.size();i++){
        for(int j=0;j<PMachine[i].pod.size();j++){
              out << QString("\"") << QString("scenario1-pod") << PMachine[i].pod[j].ID;
              out << QString("\"");
              out << QString(":") ;
              out << QString("\"") <<  PMachine[i].pod[j].IP << QString("\"") ;
              if(j!=PMachine[i].pod.size()-1){
              out << ",";
              }
              out << endl;
        }
    }
    out << "}" <<endl;


    output_file_forPod.close();
}

//业务重构
void MainWindow::Reconfiguration(){
    qDebug()<<"satData.txt read Complete!";
    qDebug()<<"Reconfiguring  ==================>>>>>>>>>>>>>>>>>";
}


//业务重构
void MainWindow::PerformanceTest(){
//    QString str = QString("/root/home/guo_scenarios_scripts")+QString("/hcw_test.py >> /home/hcew123.txt ");
//    system(str.toLatin1().data());
//    //读取data数据
//    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
//    QTextCodec::setCodecForLocale(codec);
//    QFile file("/home/hcew1234.txt");
//    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
//        qDebug()<<"Can't open the file!"<<endl;
//    }
//    while(!file.atEnd()) {
//        QByteArray line = file.readLine();
//        QString str(line);
//        str.remove("\n");
//        QString str1 = str.section("Mbps",0,0).section(":",1,1);
//        str1.remove("\n");
//        //qDebug()<<"**************"+str1 ;
//        reserved_data.append(str1.toDouble());
//    }


    //返回资源利用率

    //服务器测试峰值
}

void MainWindow::PerformanceTest_slot(){
   // PerformanceTest();
}

