#ifndef SERVEWIDGET_H
#define SERVEWIDGET_H

#include <QWidget>
#include<QTcpServer>//监听套接字
#include<QTcpSocket>//建立连接的通信套接字
#include "detect.h"

namespace Ui {
class ServeWidget;
}

class ServeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServeWidget(QWidget *parent = 0);
    ~ServeWidget();
    int usr_port = 8888;


    int data_qstring_array_to_int;
    detect s;

private slots:
    void on_ButtonSend_clicked();

    void on_ButtonClose_clicked();
    void decide_branch();

private:
    Ui::ServeWidget *ui;
    QTcpServer *tcpServer;//监听套接字
    QTcpSocket *tcpSocket;//通信套接字
};

#endif // SERVEWIDGET_H
