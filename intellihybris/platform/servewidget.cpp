#include "servewidget.h"
#include "ui_servewidget.h"

ServeWidget::ServeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServeWidget)
{
    ui->setupUi(this);

    //初始化套接字
    tcpServer = NULL;
    tcpSocket = NULL;

    //监听套接字，指定父对象自动回收空间
    tcpServer = new QTcpServer(this);

    //设定端口
    ///QHostAddress::AnyIPv4, Any, AnyIPv6
    tcpServer->listen(QHostAddress::AnyIPv4, usr_port);

    //新连接建立的信号槽
    ///Qt5的Lambda表达式
    ///Qt5的connect方式（匿名函数）：[=](){......}
    connect(tcpServer,&QTcpServer::newConnection,[=]()
    {
        //取出建立好连接的套接字
        tcpSocket = tcpServer->nextPendingConnection();

        //获取对方的IP和端口
        QString ip = tcpSocket->peerAddress().toString();
        qint16 port = tcpSocket->peerPort();

        //将信息显示到UI
        QString temp = QString("[%1:%2]:成功连接到用户任务需求模块").arg(ip).arg(port);
        ui->textEditRead->setText(temp);

        //连接建立后，读取到数据的信号槽
        connect(tcpSocket, &QTcpSocket::readyRead,
            [=]()
            {
             //从通信套接字取出内容
             QByteArray array = tcpSocket->readAll();
             //显示到UI
             ui->textEditRead->append(array);
             QString data_qstring_array = array;

             data_qstring_array_to_int = data_qstring_array.toInt();

                     qDebug() << QString::number(data_qstring_array_to_int);
                     qDebug() << QString::number(data_qstring_array_to_int);
                     ServeWidget::decide_branch();

            } );
    });




    setWindowTitle("任务管理模块");








}

ServeWidget::~ServeWidget()
{
    delete ui;
}

void ServeWidget::on_ButtonSend_clicked()
{
    if(NULL == tcpSocket)
    {return;}

    //获取编辑区内容
    QString str = "任务管理模块向任务需求模块反馈数据";

    //给对方发数据,使用的套接字为tcpSocket
    tcpSocket->write( str.toUtf8().data() );



}

void ServeWidget::on_ButtonClose_clicked()
{
    if(NULL == tcpSocket)
    {return;}

    //主动和客户端断开连接
    tcpSocket->disconnectFromHost();

    //关闭套接字
    tcpSocket->close();
    tcpSocket = NULL;

}

void ServeWidget::decide_branch()
{
    if(data_qstring_array_to_int == 1){
        qDebug() << "如果用户模块向任务管理模块发送的数据为：1";
        s.usr_detect_process();
    }
    if(data_qstring_array_to_int == 2){
        qDebug() << "如果用户模块向任务管理模块发送的数据为：2";
        s.detect_message_interrupt();


    }




}
