#ifndef ALLCONFIGURETYPE_H
#define ALLCONFIGURETYPE_H
#include <QString>
#include <QMap>
#include <QHash>
#include <QTime>
#include <QStringList>
#include <QMutex>
#include <qdebug.h>
#include <QApplication>
#include <QDesktopWidget>

#pragma pack(1)
#define TR(str)   (QString::fromLocal8Bit(str))

struct Application
    {
       //用户类型
       int type;

       //业务编号
       QString ID;

       //业务提出的CPU需求均值及方差
       double Req_CPU;
       double Req_CPU_mean;
       double Req_CPU_var;

       //业务提出的带宽需求
       double Req_Band;
       double Req_Band_mean;
       double Req_Band_var;

       //CPU的估计参数：均值、方差
       double Estimate_CPU_mean;
       double Estimate_CPU_var;
       //带宽的估计参数：均值、方差
       double Estimate_Band_mean;
       double Estimate_Band_var;

       //应用部署的服务器 IP
       QString location;

       //测量的瞬时CPU实际利用量
       double Measure_CPU;
       //测量的瞬时Band实际利用量
       double Measure_Band;

       //实际分配的资源
       double Real_CPU;
       double Real_Band;


};

extern QMap<QString, Application> id_2_application_map;

    struct Pod
    {
       //Pod所在服务器的IP
       QString IP;

       //Pod的CPU容量
       double CPU;

       //Pod的带宽大小
       double Band;

       //Pod承载的业务编号
       QString ID;

       //pod cpu测试峰值
       double cpu_peek;

       //pod 带宽测试峰值
       double bandwidth_peek;

    };

    struct PM
    {
        //物理机可用CPU及CPU总量
        double Available_CPU;
        double Capacity_CPU;

        //物理机可用带宽及带宽总量
        double Available_Band;
        double Capacity_Band;

        //物理机连接的物理交换机Port
        int conncetPort;

        //物理机IP
        QString IP;

        //物理机名称
        QString Name;

        //物理机内部署的Pod及其信息
        QVector <Pod>pod;

        //cpu资源利用率
        double cpu_utilization;

        //服务器测试峰值
        double node_peek;

        //CPU映射资源利用率
        double cpu_embedding_utilization;

        //CPU超卖比
        double cpu_oversale_rate;

        //网络资源利用率
        double net_utilization;

        //带宽测试峰值
        double bandwidth_peek;

        //网络映射资源利用率
        double net_embedding_utilization;

        //网络超卖比
        double net_oversale_rate;

    };

    struct vSwitch{

        //虚拟交换机位置
        QString location;

        //虚拟交换机的端口数量
        int port_Num;

    };

    struct Port
    {
        //端口连接的物理机IP
        QString Server_IP;

        //端口所在的Vxlan
        QString VxlanID;

        //端口流量
        double traffic;
    };

    struct Switch
    {
        //物理交换机端口数量
        int port_Num;

        //交换机端口信息
        QVector <Port>port;
    };

    extern QVector <Application>application;
    extern QVector <PM>PMachine;

#pragma pack()
#endif // ALLCONFIGURETYPE_H
