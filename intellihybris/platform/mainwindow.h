#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "detect.h"
#include "algorithm.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <QProcess>
#include <QtCore/QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QTextCodec>
#include<QTime>
#include<QtCharts>
#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QMdiSubWindow>
#include "qcustomplot.h"
#include <math.h>
#include <QMessageBox>
#include <QDebug>
#include <QTextCodec>
#include <QObject>
#include <QTimer>
#include <QUdpSocket>
#include <QVector>
#include <QByteArray>
//hcw
#include <QtWidgets/QWidget>
#include <QtCharts/QChartGlobal>
QT_BEGIN_NAMESPACE
class QComboBox;
class QCheckBox;
QT_END_NAMESPACE

QT_CHARTS_BEGIN_NAMESPACE
class QChartView;
class QChart;
QT_CHARTS_END_NAMESPACE

typedef QPair<QPointF, QString> Data;
typedef QList<Data> DataList;
typedef QList<DataList> DataTable;

QT_CHARTS_USE_NAMESPACE
//hcw
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();      //初始化

    void Estimate();  //估计
    void PolicyAlgorithm(); //决策算法
    void OutPut();    //算法输出
    void Reconfiguration(); //业务重构
    void PerformanceTest(); //metric calculation
    QVector<int> Cross;

    QTimer *timer;
    double x1=0,y1=0;

    QUdpSocket *ROuterUdpSocket;

    double x=0,y=0,z=0,i = 2;
    float var_old,var_new,mean_old,mean_new;
    double biggest,smallest;
    float t;
    float sum = 0;

    double x_3=0,i_3 = 2;
    float var_old_3,var_new_3,mean_old_3,mean_new_3;
    float t_3;
    float sum_3 = 0;

    double x_4=0,i_4 = 2;
    float var_old_4,var_new_4,mean_old_4,mean_new_4,std_old_4,std_new_4;
    float t_4;
    float sum_4 = 0;

    double x_5=0,i_5 = 2;
    float var_old_5,var_new_5,mean_old_5,mean_new_5;
    float t_5;
    float sum_5 = 0;

    double x_6=0,i_6 = 2;
    float var_old_6,var_new_6,mean_old_6,mean_new_6,std_old_6,std_new_6;
    float t_6;
    float sum_6 = 0;

    QList<int> reserved_data;
//    MainWindow w;


private:

    Ui::MainWindow *ui;
    QTimer Updater;
    QTimer timer_performance_test;
    int timeCounter = 0;

    QTimer* my_timer;
    QUdpSocket* sendpacketSocket;//
    QUdpSocket* recvpacketSocket;//

//    Ui::MainWindow *ui;
    detect Mytimer;


private slots:

    void nodeReceived();
    void showWindow();//显示整体布局
    void fill_userTable();//加载用户需求表
    void Update_Struct();
    void InPut();     //测量输入
    void PerformanceTest_slot(); //metric calculation

    void slot_timeout();
    void slot_timeout_2();
    void slot_timeout_3();
    void slot_timeout_4();
    void read_txt_function();

    void on_pushButton_clicked();

    void SimpleDemo();
};

#endif // MAINWINDOW_H
